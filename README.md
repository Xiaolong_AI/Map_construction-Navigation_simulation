# 基于ROS的机器人建图与导航仿真

#### 介绍
CSDN博客—基于机器人建图与导航仿真全过程代码

#### 软件架构
ROS


#### 安装教程

1.  ROS-Melodic
2.  建图导航功能包

#### 使用说明

1.  需要自己创建工作空间
2.  需要自己编译

#### 功能包介绍

1.  racecar_description-机器人本体文件
2.  racecar_gazebo-gazebo仿真配置
3.  rf2o_laser_odometry-三角测距功能包
4.  simple_layers-可以不使用
